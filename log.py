from datetime import datetime



def log(user, activity, fileName):
        time = datetime.now()
        timeFormat = time.strftime("%d/%m/%Y %H:%M:%S")
        userString = f'User: {user},'
        activityString = f'Activity: {activity},'
        timeString = f'Date/Time: {timeFormat},'
        userActivity = "{:30s}{:60s}{:1s}".format(userString, activityString, timeString)
        userActivity += "\n"
        file = open(fileName, 'a')
        file.write(userActivity)
        file.close()









